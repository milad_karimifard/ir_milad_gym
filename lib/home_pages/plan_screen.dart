import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PlanScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => PlanScrrenState();
}


class PlanScrrenState extends State<PlanScreen> {
  bool isScrolled = false;

  ScrollController _controller;

  @override
  void initState() {
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    super.initState();
  }

  _scrollListener() {
    if (_controller.offset > 20) {
      setState(() {
        isScrolled = true;
      });
    } else {
      setState(() {
        isScrolled = false;
      });
    }
  }

  AppBar GetMainAppbar(BuildContext context) {
    var pageSize = MediaQuery
        .of(context)
        .size;

    return AppBar(
      title: Text('Home Gym', style: TextStyle(fontSize: 35)),
      automaticallyImplyLeading: false,
      centerTitle: true,
      elevation: 0,
      backgroundColor: Color(0xff89ABD4),
      bottom: PreferredSize(
        preferredSize: Size.fromHeight(100),
        child: Container(
          child: Positioned(
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text('0',
                          style: TextStyle(
                              color: Color.fromRGBO(46, 112, 190, 0.69),
                              fontSize: 25)),
                      Text('Training',
                          style: TextStyle(
                              color: Color.fromRGBO(46, 112, 190, 0.69),
                              fontSize: 20))
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text('0',
                          style: TextStyle(
                              color: Color.fromRGBO(46, 112, 190, 0.69),
                              fontSize: 25)),
                      Text('Kcal',
                          style: TextStyle(
                              color: Color.fromRGBO(46, 112, 190, 0.69),
                              fontSize: 20))
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text('0',
                          style: TextStyle(
                              color: Color.fromRGBO(46, 112, 190, 0.69),
                              fontSize: 25)),
                      Text('Minutes',
                          style: TextStyle(
                              color: Color.fromRGBO(46, 112, 190, 0.69),
                              fontSize: 20))
                    ],
                  )
                ],
              ),
              width: pageSize.width * 0.8,
              height: pageSize.height * 0.13,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(50),
                      topRight: Radius.circular(50))),
            ),
          ),
          decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [
                  Color.fromRGBO(137, 171, 212, 1),
                  Color.fromRGBO(137, 171, 212, 0.2)
                ],
                begin: Alignment.bottomLeft,
                end: Alignment.topRight,
                stops: [0.1, 0.8]),
          ),
        ),
      ),
    );
  }

  AppBar GetSecendAppbar(BuildContext context) {
    var pageSize = MediaQuery
        .of(context)
        .size;

    return AppBar(
        elevation: 5,
        automaticallyImplyLeading: false,
        backgroundColor: Color(0xff89ABD4),
        title: Padding(
          padding: EdgeInsets.only(top: 10),
          child: Text("Home Gym",style: TextStyle(fontSize: 30)),
        ),
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(70),
          child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text('0',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20)),
                      Text('Training',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20))
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text('0',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20)),
                      Text('Kcal',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20))
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text('0',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20)),
                      Text('Minutes',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20))
                    ],
                  )
                ],
              ),
              width: pageSize.width * 0.8,
              height: pageSize.height * 0.09,
          ),
        ));
  }

  final staticImageData = [
    'assets/images/1.png',
    'assets/images/2.png',
    'assets/images/3.png',
    'assets/images/4.png'
  ];

  final staticTextData = [
    'Hands',
    'Spine',
    'Torso',
    'Legs'
  ];

  Widget ListItemBuilder(BuildContext context,String assetFile,String text){

    var pageSize = MediaQuery.of(context).size;

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 30),
      child: Stack(
        children: <Widget>[
          Container(
              width: pageSize.width * 0.8,
              height: pageSize.height * 0.25,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(25),
                  image : DecorationImage(
                      image: AssetImage(assetFile)
                  )
              )
          ),
          Positioned(
            left: pageSize.width * 0.05,
            top: pageSize.height * 0.15,
            child: Text(text,style: TextStyle(
              fontSize: 20,
              color: Color.fromRGBO(137, 171, 212, 1)
            ),),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: isScrolled ? GetSecendAppbar(context) : GetMainAppbar(context),
        body: Container(
          margin: EdgeInsets.symmetric(vertical: 20, horizontal: 15),
          child: ListView.builder(
              controller: _controller,
              itemCount: 4,
              itemBuilder: (_context, index) {
                return ListItemBuilder(context, staticImageData[index],staticTextData[index]);
              }),
        ));
  }
}