import 'package:flutter/material.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:ir_milad_gym/Models/LessonModel.dart';
import 'package:ir_milad_gym/services/lesson_services.dart';
import 'package:ir_milad_gym/component/lesson_component.dart';

class LessonScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => LessonScreenState();
}

class LessonScreenState extends State<LessonScreen> with SingleTickerProviderStateMixin {
  TabController tabController;
  List<LessonModel> _lessons = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    tabController = new TabController(initialIndex: 1, length: 4, vsync: this);
    getLessons();
  }

  getLessons() async {
    var lesson = await (new LessonService()).GetLessons();
    setState(() {
      _lessons = lesson;
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: GradientAppBar(
        title: Text('Lessons'),
        centerTitle: true,
        automaticallyImplyLeading: false,
        gradient: LinearGradient(
            colors: [
              Color.fromRGBO(110, 156, 210, 1),
              Color.fromRGBO(137, 171, 212, 0.87),
              Color.fromRGBO(110, 156, 210, 1)
            ], begin: Alignment.bottomLeft, end: Alignment.topRight),
        bottom: new TabBar(
            controller: tabController,
            indicatorColor: Colors.white,
            unselectedLabelColor: Colors.black38,
            labelColor: Colors.white,
            labelStyle: TextStyle(fontSize: 15),
            tabs: <Widget>[
              new Tab(text: "Hands"),
              new Tab(text: "Spine"),
              new Tab(text: "Torso"),
              new Tab(text: "Legs")
            ]),
      ),
      body: new TabBarView(
          controller: tabController,
          children: <Widget>[
            new LessonList(
                lessons: _lessons.where((item) => item.category == 'hands')
                    .toList()),
            new LessonList(
                lessons: _lessons.where((item) => item.category == 'spine')
                    .toList()),
            new LessonList(
                lessons: _lessons.where((item) => item.category == 'torso')
                    .toList()),
            new LessonList(
                lessons: _lessons.where((item) => item.category == 'legs')
                    .toList()),
          ]
      ),
    );
  }
}
