import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ir_milad_gym/Models/ProfileModel.dart';
import 'package:ir_milad_gym/home_pages/privacy_policy_webview.dart';
import 'package:ir_milad_gym/pages/login_screen.dart';
import 'package:ir_milad_gym/services/auth_services.dart';
import 'package:ir_milad_gym/services/profile_services.dart';

import 'gender_page_without_select.dart';

class ProfileScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => ProfileScreenState();
}

class ProfileScreenState extends State<ProfileScreen> {
  ProfileModel _profile = new ProfileModel();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getProfile();
  }

  updateProfile(String weight, String height) async {
    bool result = await (new ProfileService()).UpdateProfile(weight, height);

    if (result) {
      await getProfile();
      Navigator.of(context).pop(false);
    }
  }

  navigationToLoginPage() {
    Navigator.of(context).push(new LoginPageRoute());
  }

  navigationToGenderPage() {
    Navigator.of(context).push(new GenderPageWithoutSelectRoute());
  }

  LogoutUser() async {
    bool response = await (new AuthService()).SignOut();

    if (response) {
      navigationToLoginPage();
    }
  }

  navigationToPrivacyPage() {
    Navigator.of(context).push(new PrivacyScreenPageRoute());
  }

  getProfile() async {
    var profileModel = await (new ProfileService()).GetProfile();
    setState(() {
      _profile = profileModel;
    });
  }

  final _formKey = GlobalKey<FormState>();
  String weightInput = '';
  String heightInput = '';

  weightOnSaved(String value) {
    weightInput = value;
  }

  heightlOnSaved(String value) {
    heightInput = value;
  }

  showBiometricDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return Scaffold(
            resizeToAvoidBottomInset: false,
            body: AlertDialog(
              title: new Text('Biometric'),
              scrollable: true,
              content: Container(
                height: MediaQuery.of(context).size.height * 0.16,
                child: Column(
                  children: <Widget>[
                    Text('Please, Input Your Biometric Data'),
                    Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          TextFormField(
                              initialValue: _profile.weight,
                              onSaved: weightOnSaved,
                              decoration:
                                  new InputDecoration(hintText: 'Weight'),
                              keyboardType: TextInputType.number),
                          TextFormField(
                              initialValue: _profile.height,
                              onSaved: heightlOnSaved,
                              decoration:
                                  new InputDecoration(hintText: 'Height'),
                              keyboardType: TextInputType.number)
                        ],
                      ),
                    )
                  ],
                ),
              ),
              actions: <Widget>[
                new FlatButton(
                    onPressed: () => Navigator.of(context).pop(false),
                    child: new Text(
                      'Cancel',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color.fromRGBO(137, 171, 212, 1)),
                    )),
                new FlatButton(
                    onPressed: () {
                      _formKey.currentState.save();
                      // ToDO :
                      updateProfile(weightInput, heightInput);
                    },
                    child: new Text(
                      'OK',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color.fromRGBO(137, 171, 212, 1)),
                    )),
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    var pageSize = MediaQuery.of(context).size;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 40),
            decoration: BoxDecoration(
              gradient: LinearGradient(colors: [
                Color.fromRGBO(110, 156, 210, 1),
                Color.fromRGBO(137, 171, 212, 0.87),
                Color.fromRGBO(110, 156, 210, 1)
              ], begin: Alignment.bottomLeft, end: Alignment.topRight),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Container(
                  child: Column(
                    children: <Widget>[
                      Text(_profile.weight,
                          style: TextStyle(fontSize: 18, color: Colors.white)),
                      Text('Weight',
                          style: TextStyle(fontSize: 18, color: Colors.white))
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 10),
                  child: Column(
                    children: <Widget>[
                      Text(_profile.username,
                          style: TextStyle(color: Colors.white, fontSize: 35)),
                      Text('Male',
                          style: TextStyle(color: Colors.white, fontSize: 20))
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    children: <Widget>[
                      Text(_profile.height,
                          style: TextStyle(fontSize: 18, color: Colors.white)),
                      Text('Height',
                          style: TextStyle(fontSize: 18, color: Colors.white))
                    ],
                  ),
                )
              ],
            ),
          ),
          Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('Training rest',
                        style: TextStyle(
                            fontSize: 22,
                            color: Color.fromRGBO(137, 171, 212, 1))),
                    Text('30 sec',
                        style: TextStyle(
                            fontSize: 22,
                            color: Color.fromRGBO(137, 171, 212, 1))),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Divider(
                  height: 10,
                  color: Colors.black,
                ),
              )
            ],
          ),
          Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('Notifications',
                        style: TextStyle(
                            fontSize: 22,
                            color: Color.fromRGBO(137, 171, 212, 1))),
                    Switch(
                      onChanged: (changed) {},
                      value: true,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Divider(
                  height: 10,
                  color: Colors.black,
                ),
              )
            ],
          ),
          GestureDetector(
            onTap: () => showBiometricDialog(context),
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('Biometric',
                          style: TextStyle(
                              fontSize: 22,
                              color: Color.fromRGBO(137, 171, 212, 1)))
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Divider(
                    height: 10,
                    color: Colors.black,
                  ),
                )
              ],
            ),
          ),
          GestureDetector(
            onTap: () => navigationToGenderPage(),
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('Start dialog',
                          style: TextStyle(
                              fontSize: 22,
                              color: Color.fromRGBO(137, 171, 212, 1))),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Divider(
                    height: 10,
                    color: Colors.black,
                  ),
                )
              ],
            ),
          ),
          GestureDetector(
            onTap: () => navigationToPrivacyPage(),
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('Privacy policy',
                          style: TextStyle(
                              fontSize: 22,
                              color: Color.fromRGBO(137, 171, 212, 1))),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Divider(
                    height: 10,
                    color: Colors.black,
                  ),
                )
              ],
            ),
          ),
          GestureDetector(
            onTap: () => LogoutUser(),
            child: Container(
              child: Container(
                margin: EdgeInsets.only(top: 100, bottom: 5),
                width: pageSize.width * 0.9,
                height: 50,
                decoration: BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.rectangle,
                    border:
                        Border.all(color: Color.fromRGBO(46, 112, 190, 0.69)),
                    borderRadius: BorderRadius.circular(50)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Sign Out',
                      style: TextStyle(
                          color: Color.fromRGBO(46, 112, 190, 0.69),
                          fontSize: 30),
                    )
                  ],
                ),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 30),
            child: Column(
              children: <Widget>[
                Text('Design by Sergey Klimovich',
                    style: TextStyle(
                        color: Color.fromRGBO(46, 112, 190, 0.69),
                        fontSize: 20)),
                Text('Develop by Milad Karimifard',
                    style: TextStyle(
                        color: Color.fromRGBO(46, 112, 190, 0.69),
                        fontSize: 20)),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class PrivacyScreenPageRoute extends CupertinoPageRoute {
  PrivacyScreenPageRoute()
      : super(builder: (BuildContext context) => new PrivacyScreen());

  // OPTIONAL IF YOU WISH TO HAVE SOME EXTRA ANIMATION WHILE ROUTING
  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    return new FadeTransition(opacity: animation, child: new PrivacyScreen());
  }
}

class LoginPageRoute extends CupertinoPageRoute {
  LoginPageRoute()
      : super(builder: (BuildContext context) => new LoginScreenApp());

  // OPTIONAL IF YOU WISH TO HAVE SOME EXTRA ANIMATION WHILE ROUTING
  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    return new FadeTransition(opacity: animation, child: new LoginScreenApp());
  }
}

class GenderPageWithoutSelectRoute extends CupertinoPageRoute {
  GenderPageWithoutSelectRoute()
      : super(
            builder: (BuildContext context) =>
                new GenderSelectWithoutSelectPage());

  // OPTIONAL IF YOU WISH TO HAVE SOME EXTRA ANIMATION WHILE ROUTING
  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    return new FadeTransition(
        opacity: animation, child: new GenderSelectWithoutSelectPage());
  }
}
