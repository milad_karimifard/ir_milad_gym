import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class PrivacyScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => PrivacyScreenState();
}

class PrivacyScreenState extends State<PrivacyScreen> {

  final Completer<WebViewController> _controller =
  Completer<WebViewController>();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    var pageSize = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(137, 171, 212, 1),
        title: Text('Privacy Policy')
      ),
      body: WebView(
      initialUrl: 'https://dailyworkoutapps.com/privacy-policy.html',
      javascriptMode: JavascriptMode.unrestricted,
      onWebViewCreated: (WebViewController webViewController) {
        _controller.complete(webViewController);
      },
    ));
  }
}