class ProfileModel{
  String id;
  String username = 'loading ...';
  String email;
  String password;
  String weight = 'loading ...';
  String height = 'loading ...';

  ProfileModel(){}

  ProfileModel.fromJson(Map<String , dynamic> parsedJson) {
    id = parsedJson['id'];
    username = parsedJson['username'];
    email = parsedJson['email'];
    password = parsedJson['password'];
    weight = parsedJson['weight'];
    height = parsedJson['height'];
  }
}

//[{"id":"302","username":"milad","email":"milad@mail.com","password":"123","weight":"50","height":"180"}]