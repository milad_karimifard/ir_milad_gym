class LessonModel{
  String id;
  String url;
  String category;

  LessonModel({this.id,this.url,this.category});

  LessonModel.fromJson(Map<String , dynamic> parsedJson) {
    id = parsedJson['id'];
    url = parsedJson['url'];
    category = parsedJson['category'];
  }
}
//[{"id":"1","url":"https://youtu.be/vK_vQLSAUeE","category":"torso"}
