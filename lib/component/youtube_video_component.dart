import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class YoutubeVideoComponent extends StatelessWidget {

  final youtubeLink;
  YoutubeVideoComponent(this.youtubeLink);

  openUrl() async {
    String id = youtubeLink.substring(17);
    await launch('https://www.youtube.com/watch?v='+id, forceSafariVC: false);
  }

  @override
  Widget build(BuildContext context) {
    var pageSize = MediaQuery
        .of(context)
        .size;

    return GestureDetector(
      onTap: () {
        openUrl();
      },
      child: Container(
          width: pageSize.width * 0.75,
          height: pageSize.height * 0.2,
          margin: EdgeInsets.symmetric(vertical: 10),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(25),
              image : DecorationImage(
                  fit: BoxFit.cover,
                  image: NetworkImage(getTumnileImage(youtubeLink))
              )
          )
      ),
    );
  }

  getTumnileImage(String youtubeLink) {
    String id = youtubeLink.substring(17);
    return 'https://img.youtube.com/vi/$id/0.jpg';
  }

}