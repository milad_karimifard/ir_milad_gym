import 'package:flutter/material.dart';
import 'package:ir_milad_gym/Models/LessonModel.dart';
import 'package:ir_milad_gym/component/youtube_video_component.dart';

class LessonList extends StatelessWidget {
  final List<LessonModel> lessons;

  LessonList({this.lessons});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20,horizontal: 15),
      child: ListView.builder(
          scrollDirection: Axis.vertical,
          itemCount: lessons.length,
          itemBuilder: (context, index) {
            return YoutubeVideoComponent(this.lessons[index].url);
          }),
    );
  }
}