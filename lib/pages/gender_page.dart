import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ir_milad_gym/pages/woman_select.dart';

import 'man_select.dart';


class GenderSelectPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => GenderSelectState();
}

class GenderSelectState extends State<GenderSelectPage> {
  navigationPage() {
    if (choosedGender == 'male') {
      Navigator.of(context).push(new ManPageRoute());
    } else if (choosedGender == 'female') {
      Navigator.of(context).push(new WomanPageRoute());
    }
  }

  // 2 mode -> male/female
  String choosedGender = '';
  Color manBottomColor = Colors.white;
  Color womanBottomColor = Colors.white;

  selectGender(String gender) {
    if (gender == 'male') {
      manBottomColor = Color(0xffE3DFBC);
      womanBottomColor = Colors.white;
      choosedGender = 'male';
    } else if (gender == 'female') {
      womanBottomColor = Color(0xffE3DFBC);
      manBottomColor = Colors.white;
      choosedGender = 'female';
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    var pageSize = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Container(
                  width: pageSize.width,
                  height: pageSize.height,
                  color: Color.fromRGBO(46, 112, 190, 0.69)),
            ],
          ),
          Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: Text('Step 2/5',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 55,
                            fontWeight: FontWeight.bold)),
                    margin: EdgeInsets.only(bottom: 20),
                  ),
                  Container(
                    child: Text('What is your gender?',
                        style: TextStyle(color: Colors.white, fontSize: 22)),
                    margin: EdgeInsets.only(top: 20, bottom: 50),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 30, bottom: 80),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  selectGender('female');
                                });
                              },
                              child: Container(
                                width: 80,
                                height: 80,
                                margin: EdgeInsets.only(bottom: 20),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: womanBottomColor),
                                child: new Container(
                                  width: 50,
                                  height: 50,
                                  decoration: new BoxDecoration(
                                      image: new DecorationImage(
                                          image: new AssetImage(
                                              "assets/images/icons-gender_two.png"),
                                          fit: BoxFit.cover)),
                                ),
                              ),
                            ),
                            Text(
                              'Female',
                              style: TextStyle(color: Colors.white, fontSize: 30),
                            )
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  selectGender('male');
                                });
                              },
                              child: Container(
                                width: 80,
                                height: 80,
                                margin: EdgeInsets.only(bottom: 20),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle, color: manBottomColor),
                                child: new Container(
                                  width: 50,
                                  height: 50,
                                  decoration: new BoxDecoration(
                                      image: new DecorationImage(
                                          image: new AssetImage(
                                              "assets/images/icons-gender_one.png"),
                                          fit: BoxFit.cover)),
                                ),
                              ),
                            ),
                            Text(
                              'Male',
                              style: TextStyle(color: Colors.white, fontSize: 30),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () => navigationPage(),
                    child: Container(
                      margin: EdgeInsets.only(top: 20),
                      width: 300,
                      height: 90,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.circular(50)),
                      child: new Center(
                        child: Text('Next',
                            style: TextStyle(
                                color: Color.fromRGBO(46, 112, 190, 0.69),
                                fontSize: 30,
                                fontWeight: FontWeight.bold)),
                      ),
                    ),
                  )
                ],
              ))
        ],
      ),
    );
  }
}

class ManPageRoute extends CupertinoPageRoute {
  ManPageRoute()
      : super(builder: (BuildContext context) => new ManSelectApp());

  // OPTIONAL IF YOU WISH TO HAVE SOME EXTRA ANIMATION WHILE ROUTING
  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    return new FadeTransition(opacity: animation, child: new ManSelectApp());
  }
}

class WomanPageRoute extends CupertinoPageRoute {
  WomanPageRoute()
      : super(builder: (BuildContext context) => new WomanSelectApp());

  // OPTIONAL IF YOU WISH TO HAVE SOME EXTRA ANIMATION WHILE ROUTING
  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    return new FadeTransition(opacity: animation, child: new WomanSelectApp());
  }
}
