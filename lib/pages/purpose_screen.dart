import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'gender_page.dart';

class PurposeScreenApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => PurposeScreenState();
}

class PurposeScreenState extends State<PurposeScreenApp>{

  navigationPage() {
    Navigator.of(context).push(new GenderPageRoute());
  }

  saveData(String key,String data) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(key, data);
  }

  Color currentBottomColor = Colors.white;

  selectButtomColor(){
    setState(() {
      currentBottomColor  = Color(0xffE3DFBC);
    });
  }
  deSelectButtomColor(){
    setState(() {
      currentBottomColor  = Colors.white;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    saveData('userFirst', 'no');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    var pageSize = MediaQuery.of(context).size;

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Container(
                  width: pageSize.width,
                  height: pageSize.height,
                  color: Color.fromRGBO(46, 112, 190, 0.69)
              ),
            ],
          ),
          Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: Text('Step 1/5',style: TextStyle(color: Colors.white,fontSize: 55,fontWeight: FontWeight.bold)),
                    margin: EdgeInsets.only(bottom: 20),
                  ),
                  Container(
                    child: Text('What is your purpose?',style: TextStyle(color: Colors.white,fontSize: 22)),
                    margin: EdgeInsets.only(top: 20,bottom: 60),
                  ),
                  GestureDetector(
                    onLongPressStart: selectButtomColor(),
                    onLongPressEnd: deSelectButtomColor(),
                    onTap: () => navigationPage(),
                    child: Container(
                      margin: EdgeInsets.only(top: 20,bottom: 15),
                      width: 300,
                      height:90,
                      decoration: BoxDecoration(
                          color: currentBottomColor,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.circular(50)
                      ),
                      child: Row(
                        children: <Widget>[
                          new Container(
                            margin: EdgeInsets.only(left: 50,right: 20),
                            width: 40,
                            height: 40,
                            decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    image: new AssetImage("assets/images/icons-scale.png"),
                                    fit: BoxFit.cover)),
                          ),
                          Text('Weight loss',style: TextStyle(color: Color.fromRGBO(46, 112, 190, 0.69),fontSize: 25),)
                        ],
                      ),
                    ),
                  ),
                  GestureDetector(
                    child: Container(
                      margin: EdgeInsets.only(top: 20,bottom: 15),
                      width: 300,
                      height:90,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.circular(50)
                      ),
                      child: Row(
                        children: <Widget>[
                          new Container(
                            margin: EdgeInsets.only(left: 50,right: 20),
                            width: 40,
                            height: 40,
                            decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    image: new AssetImage("assets/images/icons-prenatal-yoga.png"),
                                    fit: BoxFit.cover)),
                          ),
                          Text('Keeping fit',style: TextStyle(color: Color.fromRGBO(46, 112, 190, 0.69),fontSize: 25),)
                        ],
                      ),
                    ),
                  ),
                  GestureDetector(
                    child: Container(
                      margin: EdgeInsets.only(top: 20),
                      width: 300,
                      height:90,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.circular(50)
                      ),
                      child: Row(
                        children: <Widget>[
                          new Container(
                            margin: EdgeInsets.only(left: 50,right: 20),
                            width: 40,
                            height: 40,
                            decoration: new BoxDecoration(
                                image: new DecorationImage(
                                    image: new AssetImage("assets/images/icons-triceps.png"),
                                    fit: BoxFit.cover)),
                          ),
                          Text('Build muscle',style: TextStyle(color: Color.fromRGBO(46, 112, 190, 0.69),fontSize: 25),)
                        ],
                      ),
                    ),
                  )
                ],
              )
          )
        ],
      ),
    );
  }
}

class GenderPageRoute extends CupertinoPageRoute {
  GenderPageRoute()
      : super(builder: (BuildContext context) => new GenderSelectPage());


  // OPTIONAL IF YOU WISH TO HAVE SOME EXTRA ANIMATION WHILE ROUTING
  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    return new FadeTransition(opacity: animation, child: new GenderSelectPage());
  }
}
