import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ir_milad_gym/pages/weight_page.dart';

import 'gender_page.dart';

class ExcrciseTimeApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => ExcrciseTimeState();
}

class ExcrciseTimeState extends State<ExcrciseTimeApp> {
  Color currentBottomColor = Colors.white;

  // 2 mode -> newbie/keepon/advance
  String choosedExercise = '';
  Color newbieBottomColor = Colors.white;
  Color keeponBottomColor = Colors.white;
  Color advanceBottomColor = Colors.white;

  selectExercise(String selected) {
    if (selected == 'newbie') {
      newbieBottomColor = Color(0xffE3DFBC);
      keeponBottomColor = Colors.white;
      advanceBottomColor = Colors.white;
      choosedExercise = 'newbie';
    } else if (selected == 'keepon') {
      newbieBottomColor = Colors.white;
      keeponBottomColor = Color(0xffE3DFBC);
      advanceBottomColor = Colors.white;
      choosedExercise = 'keepon';
    } else if (selected == 'advance') {
      newbieBottomColor = Colors.white;
      keeponBottomColor = Colors.white;
      advanceBottomColor = Color(0xffE3DFBC);
      choosedExercise = 'advance';
    }
  }

  navigationPage() {
    Navigator.of(context).push(new SecondPageRoute());
  }

  selectButtomColor() {
    setState(() {
      currentBottomColor = Color(0xffE3DFBC);
    });
  }

  deSelectButtomColor() {
    setState(() {
      currentBottomColor = Colors.white;
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    var pageSize = MediaQuery.of(context).size;

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Container(
                  width: pageSize.width,
                  height: pageSize.height,
                  color: Color.fromRGBO(46, 112, 190, 0.69)),
            ],
          ),
          Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: Text('Step 4/5',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 55,
                            fontWeight: FontWeight.bold)),
                    margin: EdgeInsets.only(bottom: 20),
                  ),
                  Container(
                    child: Text('How often do you exercise??',
                        style: TextStyle(color: Colors.white, fontSize: 22)),
                    margin: EdgeInsets.only(top: 20, bottom: 25),
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        selectExercise('newbie');
                      });
                    },
                    child: Container(
                      margin: EdgeInsets.only(top: 20, bottom: 5),
                      width: 300,
                      height: 90,
                      decoration: BoxDecoration(
                          color: newbieBottomColor,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.circular(50)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Newbie',
                            style: TextStyle(
                                color: Color.fromRGBO(46, 112, 190, 0.69),
                                fontSize: 25),
                          ),
                          Text(
                            'Just getting started',
                            style: TextStyle(
                                color: Color.fromRGBO(46, 112, 190, 0.69),
                                fontSize: 20),
                          )
                        ],
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        selectExercise('keepon');
                      });
                    },
                    child: Container(
                      margin: EdgeInsets.only(top: 15, bottom: 5),
                      width: 300,
                      height: 90,
                      decoration: BoxDecoration(
                          color: keeponBottomColor,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.circular(50)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Keep on',
                            style: TextStyle(
                                color: Color.fromRGBO(46, 112, 190, 0.69),
                                fontSize: 25),
                          ),
                          Text(
                            '1-2 times a week',
                            style: TextStyle(
                                color: Color.fromRGBO(46, 112, 190, 0.69),
                                fontSize: 20),
                          )
                        ],
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        selectExercise('advance');
                      });
                    },
                    child: Container(
                      margin: EdgeInsets.only(top: 15, bottom: 25),
                      width: 300,
                      height: 90,
                      decoration: BoxDecoration(
                          color: advanceBottomColor,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.circular(50)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Advanced',
                            style: TextStyle(
                                color: Color.fromRGBO(46, 112, 190, 0.69),
                                fontSize: 25),
                          ),
                          Text(
                            'More than 3 times a week',
                            style: TextStyle(
                                color: Color.fromRGBO(46, 112, 190, 0.69),
                                fontSize: 20),
                          )
                        ],
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () => navigationPage(),
                    child: Container(
                      margin: EdgeInsets.only(top: 30),
                      width: 300,
                      height: 90,
                      decoration: BoxDecoration(
                          color: currentBottomColor,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.circular(50)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Next',
                            style: TextStyle(
                                color: Color.fromRGBO(46, 112, 190, 0.69),
                                fontSize: 25),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ))
        ],
      ),
    );
  }
}

class SecondPageRoute extends CupertinoPageRoute {
  SecondPageRoute()
      : super(builder: (BuildContext context) => new WeightInputPage());

  // OPTIONAL IF YOU WISH TO HAVE SOME EXTRA ANIMATION WHILE ROUTING
  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    return new FadeTransition(
        opacity: animation, child: new WeightInputPage());
  }
}
