import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'excrcise_time.dart';


class ManSelectApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => ManSelectState();
}

class ManSelectState extends State<ManSelectApp> {
  Color currentBottomColor = Colors.white;

  navigationPage() {
    Navigator.of(context).push(new SecondPageRoute());
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    var pageSize = MediaQuery.of(context).size;

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Container(
                  width: pageSize.width,
                  height: pageSize.height,
                  color: Color.fromRGBO(46, 112, 190, 0.69)),
            ],
          ),
          Positioned(
            left: pageSize.width * 0.1,
            top: pageSize.height * 0.05,
            child: Column(
              children: <Widget>[
                Container(
                  child: Text('Step 3/5',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 55,
                          fontWeight: FontWeight.bold)),
                  margin: EdgeInsets.only(bottom: 20),
                ),
                Container(
                  child: Text('What do you want to work on??',
                      style: TextStyle(color: Colors.white, fontSize: 22)),
                  margin: EdgeInsets.only(top: 20),
                )
              ],
            ),
          ),
          Positioned(
            top: pageSize.height * 0.3,
            left: pageSize.width * 0.5,
            child: Row(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Container(
                        width: 200,
                        height: 500,
                        decoration: new BoxDecoration(
                            image: new DecorationImage(
                                image: new AssetImage("assets/images/man-select.png"),
                                fit: BoxFit.fitHeight))
                    )
                  ],
                )
              ],
            ),
          ),
          Positioned(
              top: pageSize.height * 0.39,
              left: pageSize.width * 0.15,
              child: GestureDetector(
                onTap: ()=> navigationPage(),
                child: Container(
                  margin: EdgeInsets.only(top: 20,bottom: 15),
                  width: 150,
                  height:50,
                  decoration: BoxDecoration(
                      color: currentBottomColor,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(50)
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text('Hand',style: TextStyle(color: Color.fromRGBO(46, 112, 190, 0.69),fontSize: 20,fontWeight: FontWeight.bold),)
                    ],
                  ),
                ),
              )
          ),
          Positioned(
              top: pageSize.height * 0.49,
              left: pageSize.width * 0.15,
              child: Container(
                margin: EdgeInsets.only(top: 20,bottom: 15),
                width: 150,
                height:50,
                decoration: BoxDecoration(
                    color: currentBottomColor,
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.circular(50)
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('Spine',style: TextStyle(color: Color.fromRGBO(46, 112, 190, 0.69),fontSize: 20,fontWeight: FontWeight.bold),)
                  ],
                ),
              )
          ),
          Positioned(
              top: pageSize.height * 0.59,
              left: pageSize.width * 0.15,
              child: Container(
                margin: EdgeInsets.only(top: 20,bottom: 15),
                width: 150,
                height:50,
                decoration: BoxDecoration(
                    color: currentBottomColor,
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.circular(50)
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('Torso',style: TextStyle(color: Color.fromRGBO(46, 112, 190, 0.69),fontSize: 20,fontWeight: FontWeight.bold),)
                  ],
                ),
              )
          ),
          Positioned(
              top: pageSize.height * 0.69,
              left: pageSize.width * 0.15,
              child: Container(
                margin: EdgeInsets.only(top: 20,bottom: 15),
                width: 150,
                height:50,
                decoration: BoxDecoration(
                    color: currentBottomColor,
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.circular(50)
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('Legs',style: TextStyle(color: Color.fromRGBO(46, 112, 190, 0.69),fontSize: 20,fontWeight: FontWeight.bold),)
                  ],
                ),
              )
          ),

        ],
      ),
    );
  }
}

class SecondPageRoute extends CupertinoPageRoute {
  SecondPageRoute()
      : super(builder: (BuildContext context) => new ExcrciseTimeApp());

  // OPTIONAL IF YOU WISH TO HAVE SOME EXTRA ANIMATION WHILE ROUTING
  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    return new FadeTransition(
        opacity: animation, child: new ExcrciseTimeApp());
  }
}
