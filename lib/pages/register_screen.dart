import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ir_milad_gym/pages/HomePage.dart';
import 'package:ir_milad_gym/services/auth_services.dart';
import 'package:shared_preferences/shared_preferences.dart';


class RegisterScreenApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => RegisterScreenState();
}

class RegisterScreenState extends State<RegisterScreenApp> {
  navigationPage() {
    Navigator.of(context).push(new ManPageRoute());
  }

  Future<String> ReadFromMemory(String key) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(key);
  }

  RegisterUser(String username, String email, String password) async {

    String weight = await ReadFromMemory('weight');
    String height = await ReadFromMemory('height');

    bool response = await (new AuthService()).Register({
      "username": username,
      "email": email,
      "password": password,
      "weight": weight,
      "height": height
    });

    if(response){
      navigationPage();
    }
  }

  final _formKey = GlobalKey<FormState>();
  String usernameInput = '';
  String passwordInput = '';
  String repasswordInput = '';
  String emailInput = '';

  usernameOnSaved(String value) {
    usernameInput = value;
  }

  emailOnSaved(String value) {
    emailInput = value;
  }

  passwordOnSaved(String value) {
    passwordInput = value;
  }

  repasswordOnSaved(String value) {
    repasswordInput = value;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    var pageSize = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Container(
                  width: pageSize.width,
                  height: pageSize.height,
                  color: Color.fromRGBO(46, 112, 190, 0.69)),
            ],
          ),
          Center(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                  width: 180,
                  height: 150,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          image: AssetImage("assets/icon/icon.png"),
                          fit: BoxFit.cover))),
              Container(
                child: Text('Home',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 40,
                        fontWeight: FontWeight.bold)),
                margin: EdgeInsets.only(bottom: 0),
              ),
              Container(
                child: Text('Gym',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 40,
                        fontWeight: FontWeight.bold)),
                margin: EdgeInsets.only(bottom: 5),
              ),
              Container(
                  margin: EdgeInsets.only(top: 30, bottom: 10),
                  child: Container(
                    margin: const EdgeInsets.symmetric(horizontal: 20),
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: new Column(
                      children: <Widget>[
                        new Form(
                          key: _formKey,
                          child: new Column(
                            children: <Widget>[
                              new InputFieldArea(
                                  hint: "Username",
                                  obscure: false,
                                  keyboardType: TextInputType.text,
                                  icon: Icons.person,
                                  validator: (String value) {
                                    if (value == '') {
                                      return 'The username must input';
                                    }
                                  },
                                  onSaved: usernameOnSaved),
                              new InputFieldArea(
                                  hint: "Email",
                                  obscure: false,
                                  keyboardType: TextInputType.text,
                                  icon: Icons.mail,
                                  validator: (String value) {
                                    if (value == '') {
                                      return 'The email must input';
                                    } else if (!value.contains('@')) {
                                      return 'The email is not correct';
                                    }
                                  },
                                  onSaved: emailOnSaved),
                              new InputFieldArea(
                                  hint: "Password",
                                  obscure: true,
                                  icon: Icons.lock,
                                  keyboardType: TextInputType.text,
                                  validator: (String value) {
                                    if (value == '') {
                                      return 'The password must input';
                                    }
                                  },
                                  onSaved: passwordOnSaved),
                              new InputFieldArea(
                                  hint: "Re Password",
                                  obscure: true,
                                  icon: Icons.lock,
                                  keyboardType: TextInputType.text,
                                  validator: (String value) {
                                    if (value == '') {
                                      return 'The re password must input';
                                    } else if (repasswordInput !=
                                        passwordInput) {
                                      return 'Re password is not match';
                                    }
                                  },
                                  onSaved: repasswordOnSaved)
                            ],
                          ),
                        )
                      ],
                    ),
                  )),
              GestureDetector(
                onTap: () {
                  if (_formKey.currentState.validate()) {
                    _formKey.currentState.save();
                    if (_formKey.currentState.validate()) {
                      RegisterUser(usernameInput, emailInput, passwordInput);
                    }
                  }
                },
                child: Container(
                  margin: EdgeInsets.only(top: 5),
                  width: 300,
                  height: 50,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(50)),
                  child: new Center(
                    child: Text('Sign In',
                        style: TextStyle(
                            color: Color.fromRGBO(46, 112, 190, 0.69),
                            fontSize: 30,
                            fontWeight: FontWeight.bold)),
                  ),
                ),
              )
            ],
          ))
        ],
      ),
    );
  }
}

class ManPageRoute extends CupertinoPageRoute {
  ManPageRoute() : super(builder: (BuildContext context) => new HomeApp());

  // OPTIONAL IF YOU WISH TO HAVE SOME EXTRA ANIMATION WHILE ROUTING
  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    return new FadeTransition(opacity: animation, child: new HomeApp());
  }
}

class InputFieldArea extends StatelessWidget {
  final String hint;
  final bool obscure;
  final IconData icon;
  final TextInputType keyboardType;
  final validator;
  final onSaved;

  InputFieldArea(
      {this.hint,
      this.obscure,
      this.icon,
      this.validator,
      this.onSaved,
      this.keyboardType});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Container(
      margin: const EdgeInsets.only(bottom: 10),
      height: 70,
      decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(25),
          color: Colors.white),
      child: new TextFormField(
        validator: validator,
        keyboardType: keyboardType,
        onSaved: onSaved,
        obscureText: obscure,
        style: const TextStyle(color: Colors.grey),
        decoration: new InputDecoration(
            alignLabelWithHint: true,
            border: InputBorder.none,
            focusedBorder: InputBorder.none,
            icon: Container(
              margin: EdgeInsets.only(left: 25),
              child: new Icon(
                icon,
                color: Colors.grey,
                size: 35,
              ),
            ),
            errorStyle: new TextStyle(color: Colors.redAccent, fontSize: 17),
            errorBorder: new UnderlineInputBorder(
                borderSide: new BorderSide(color: Colors.redAccent)),
            focusedErrorBorder: new UnderlineInputBorder(
                borderSide: new BorderSide(color: Colors.redAccent)),
            hintText: hint,
            hintStyle: const TextStyle(color: Colors.grey, fontSize: 20),
            contentPadding:
                const EdgeInsets.only(top: 15, right: 0, bottom: 20, left: 5)),
      ),
    );
  }
}
