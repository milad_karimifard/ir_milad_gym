import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ir_milad_gym/pages/HomePage.dart';
import 'package:ir_milad_gym/pages/login_screen.dart';
import 'package:ir_milad_gym/pages/purpose_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new SplashScreenState();
}

class SplashScreenState extends State<SplashScreen> {

  Future<String> ReadFromMemory(String key) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(key);
  }

  navigationPage() async {
    String userFirst = await ReadFromMemory('userFirst');
    String token = await ReadFromMemory('loginToken');

    if(token != '' && token != null){
      Navigator.of(context).push(new GoToHomePageRoute());
    } else if(userFirst == 'no' && userFirst != null){
      Navigator.of(context).push(new GoToLoginPageRoute());
    } else {
      Navigator.of(context).push(new GoToPurposeScreenPageRoute());
    }
  }

  @override
  Widget build(BuildContext context) {
    var pageSize = MediaQuery.of(context).size;

    // TODO: implement build
    return Scaffold(
        body: GestureDetector(
          onTap: ()=> navigationPage(),
          child: Stack(
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Container(
                    width: pageSize.width,
                    height: pageSize.height,
                    decoration: new BoxDecoration(
                        image: new DecorationImage(
                            image: new AssetImage("assets/images/splash_image.png"),
                            fit: BoxFit.cover)),
                  ),
                ],
              ),
              Positioned(
                left: pageSize.width * 0.35,
                top: pageSize.height * 0.13,
                child: Container(
                    width: 120,
                    height: 120,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.white,
                        image: DecorationImage(
                            image: AssetImage("assets/icon/icon.png"),
                            fit: BoxFit.cover))),
              ),
            ],
          ),
        ));
  }
}


class GoToPurposeScreenPageRoute extends CupertinoPageRoute {
  GoToPurposeScreenPageRoute()
      : super(builder: (BuildContext context) => new PurposeScreenApp());


  // OPTIONAL IF YOU WISH TO HAVE SOME EXTRA ANIMATION WHILE ROUTING
  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    return new FadeTransition(opacity: animation, child: new PurposeScreenApp());
  }
}

class GoToLoginPageRoute extends CupertinoPageRoute {
  GoToLoginPageRoute()
      : super(builder: (BuildContext context) => new LoginScreenApp());


  // OPTIONAL IF YOU WISH TO HAVE SOME EXTRA ANIMATION WHILE ROUTING
  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    return new FadeTransition(opacity: animation, child: new LoginScreenApp());
  }
}

class GoToHomePageRoute extends CupertinoPageRoute {
  GoToHomePageRoute()
      : super(builder: (BuildContext context) => new HomeApp());


  // OPTIONAL IF YOU WISH TO HAVE SOME EXTRA ANIMATION WHILE ROUTING
  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    return new FadeTransition(opacity: animation, child: new HomeApp());
  }
}