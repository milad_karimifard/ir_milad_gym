import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ir_milad_gym/pages/login_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';


class WeightInputPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => WeightInputState();
}

class WeightInputState extends State<WeightInputPage> {
  navigationPage() {
    Navigator.of(context).push(new ManPageRoute());
  }

  final _formKey = GlobalKey<FormState>();
  String weightInput = '';
  String heightInput = '';

  weightOnSaved(String value) {
    weightInput = value;
  }

  heightOnSaved(String value) {
    heightInput = value;
  }

  saveData(String key,String data) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(key, data);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    var pageSize = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Container(
                  width: pageSize.width,
                  height: pageSize.height,
                  color: Color.fromRGBO(46, 112, 190, 0.69)),
            ],
          ),
          Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: Text('Step 5/5',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 55,
                            fontWeight: FontWeight.bold)),
                    margin: EdgeInsets.only(bottom: 20),
                  ),
                  Container(
                    child: Text('What’s your height and weight?',
                        style: TextStyle(color: Colors.white, fontSize: 22)),
                    margin: EdgeInsets.only(top: 20, bottom: 50),
                  ),
                  Container(
                      margin: EdgeInsets.only(top: 30, bottom: 80),
                      child: Container(
                        margin: const EdgeInsets.symmetric(horizontal: 20),
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: new Column(
                          children: <Widget>[
                            new Form(
                              key: _formKey,
                              child: new Column(
                                children: <Widget>[
                                  new InputFieldArea(
                                      hint: "Height",
                                      obscure: false,
                                      validator: (String value) {
                                        if (value == '') {
                                          return 'The height is not valid';
                                        }
                                      },
                                      onSaved: heightOnSaved),
                                  new InputFieldArea(
                                      hint: "Weight",
                                      obscure: false,
                                      validator: (String value) {
                                        if (value == '') {
                                          return 'The wieght is not valid';
                                        }
                                      },
                                      onSaved: weightOnSaved),
                                ],
                              ),
                            )
                          ],
                        ),
                      )),
                  GestureDetector(
                    onTap: () async  {
                      if (_formKey.currentState.validate()) {
                        _formKey.currentState.save();
                        await saveData('height', heightInput);
                        await saveData('weight', weightInput);
                        navigationPage();
                      }
                    },
                    child: Container(
                      margin: EdgeInsets.only(top: 20),
                      width: 300,
                      height: 90,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.circular(50)),
                      child: new Center(
                        child: Text('Next',
                            style: TextStyle(
                                color: Color.fromRGBO(46, 112, 190, 0.69),
                                fontSize: 30,
                                fontWeight: FontWeight.bold)),
                      ),
                    ),
                  )
                ],
              ))
        ],
      ),
    );
  }
}

class ManPageRoute extends CupertinoPageRoute {
  ManPageRoute() : super(builder: (BuildContext context) => new LoginScreenApp());

  // OPTIONAL IF YOU WISH TO HAVE SOME EXTRA ANIMATION WHILE ROUTING
  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    return new FadeTransition(opacity: animation, child: new LoginScreenApp());
  }
}

class InputFieldArea extends StatelessWidget {
  final String hint;
  final bool obscure;
  final IconData icon;
  final validator;
  final onSaved;

  InputFieldArea(
      {this.hint, this.obscure, this.icon, this.validator, this.onSaved});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Container(
      margin: const EdgeInsets.only(bottom: 10),
      decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(35),
          color: Colors.white
      ),
      child: new TextFormField(
        validator: validator,
        keyboardType: TextInputType.number,
        onSaved: onSaved,
        obscureText: obscure,
        style: const TextStyle(color: Colors.grey),
        decoration: new InputDecoration(
            border: InputBorder.none,
            focusedBorder: InputBorder.none,
            icon: new Icon(
                icon,
                color: Colors.grey
            ),
            errorStyle: new TextStyle(color: Colors.redAccent,fontSize: 20),
            errorBorder: new UnderlineInputBorder(
                borderSide: new BorderSide(color: Colors.redAccent)),
            focusedErrorBorder: new UnderlineInputBorder(
                borderSide: new BorderSide(color: Colors.redAccent)),
            hintText: hint,
            hintStyle: const TextStyle(color: Colors.grey, fontSize: 25),
            contentPadding:
            const EdgeInsets.only(top: 15, right: 0, bottom: 20, left: 5)),
      ),
    );
  }
}
