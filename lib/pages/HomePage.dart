import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ir_milad_gym/home_pages/lesson_screen.dart';
import 'package:ir_milad_gym/home_pages/plan_screen.dart';
import 'package:ir_milad_gym/home_pages/profile_screen.dart';
import 'package:ir_milad_gym/home_pages/report_screen.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HomeApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => HomeAppState();
}

class HomeAppState extends State<HomeApp> {

  final Map<int , Widget> childrenTab = <int , Widget>{
    0 : new PlanScreen(),
    1 : new LessonScreen(),
    2: new ReportScreen(),
    3 : new ProfileScreen(),
  };

  int currentTab = 0;

  changeCurrentTab(int index){
    setState(() {
      currentTab = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return WillPopScope(
      child: Scaffold(
        body: childrenTab[currentTab],
        bottomNavigationBar: BottomNavigationBar(
          items: [
            BottomNavigationBarItem(icon: Container(width: 30,height: 30,decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/icon/1.png')))),title: Text('Plan',style: TextStyle(color: Colors.blueAccent))),
            BottomNavigationBarItem(icon: Container(width: 30,height: 30,decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/icon/2.png'))),),title: Text('Lessons',style: TextStyle(color: Colors.blueAccent))),
            BottomNavigationBarItem(icon: Container(width: 30,height: 30,decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/icon/3.png'))),),title: Text('Report',style: TextStyle(color: Colors.blueAccent))),
            BottomNavigationBarItem(icon:  Container(width: 30,height: 30,decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/icon/4.png'))),),title: Text('Report',style: TextStyle(color: Colors.blueAccent))),
          ],
          onTap: changeCurrentTab,
          currentIndex: currentTab,
        ),
      ),
      onWillPop: () {
        exit(0);
      },
    );
  }
}


