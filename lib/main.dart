import 'package:flutter/material.dart';
import 'package:ir_milad_gym/pages/HomePage.dart';
import 'package:ir_milad_gym/pages/splash_screen.dart';

void main() {
  runApp(App());
}

class App extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
        title: 'Gym App Iran',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
            fontFamily: 'ND Astroneer'
        ),
        initialRoute: "/splash_screen",
        routes: {
          "/" : (context) => HomeApp(),
          "/splash_screen" : (context) => SplashScreen()
        }
    );
  }
}
