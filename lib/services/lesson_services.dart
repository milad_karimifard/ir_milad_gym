import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:ir_milad_gym/Models/LessonModel.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LessonService {

  Future<List<LessonModel>> GetLessons() async {
    final response = await http.get('http://gym.areas.su/lessons');
    var responseBody = json.decode(response.body);

    List<LessonModel> lessons= [];
    responseBody.forEach((item){
      lessons.add(LessonModel.fromJson(item));
    });

    return lessons;
  }
}