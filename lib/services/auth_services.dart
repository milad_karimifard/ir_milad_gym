import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class AuthService {

  Future<String> Login(Map body,String username) async {
    final response = await http.post('http://gym.areas.su/signin' ,body : body);

    var responseBody = json.decode(response.body);
    var _json = json.encode(responseBody['notice']);
    var result = json.decode(_json);

    if(result['token'] != ''){
      await saveData('loginToken', result['token'].toString());
      await saveData('userName', username);
      return '';
    }else{
      return result['answer'];
    }
  }

  Future<bool> Register(Map body) async {
    final response = await http.post('http://gym.areas.su/signup' ,body : body);

    var responseBody = json.decode(response.body);
    var _json = json.encode(responseBody['notice']);
    var result = json.decode(_json);

    if(result['answer'] == 'Success'){
      return true;
    }else{
      return false;
    }
  }

  Future<bool> SignOut() async {

    String username = await ReadDataFromMemory('userName');

    final response = await http.post('http://gym.areas.su/signout' ,body : {
      "username": username,
    });

    var responseBody = json.decode(response.body);
    var _json = json.encode(responseBody['notice']);
    var result = json.decode(_json);

    if(result['text'] == 'User log out'){
      await saveData('loginToken','');
      await saveData('userName','');
      return true;
    }else{
      return false;
    }
  }

  Future<String> ReadDataFromMemory(String key) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(key);
  }

  saveData(String key,String data) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(key, data);
  }

}