import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:ir_milad_gym/Models/ProfileModel.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileService {

  Future<ProfileModel> GetProfile() async {

    String token = await ReadDataFromMemory('loginToken');

    final response = await http.post('http://gym.areas.su/profile',body: {
      'token': token
    });
    var responseBody = json.decode(response.body);


    List<ProfileModel> profile= [];
    responseBody.forEach((item){
      profile.add(ProfileModel.fromJson(item));
    });

    return profile.first;
  }

  UpdateProfile(String weight,String height) async {

    String token = await ReadDataFromMemory('loginToken');

    final response = await http.put('http://gym.areas.su/editeprofile',body: {
      'token': token,
      'weight': weight,
      'height' : height
    });

    var responseBody = json.decode(response.body);
    var _json = json.encode(responseBody['notice']);
    var result = json.decode(_json);

    if(result['text'] == 'User update'){
      return true;
    }else{
      return false;
    }
  }

  Future<String> ReadDataFromMemory(String key) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(key);
  }
}

